"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initial = initial;
exports.addPostMethod = addPostMethod;
exports.addPostMethodBufferResponse = addPostMethodBufferResponse;
exports.addGetMethod = addGetMethod;
exports.addGetMethodBufferResponse = addGetMethodBufferResponse;
exports.addGetMethodRawResponse = addGetMethodRawResponse;
exports.addDownloadGetMethod = addDownloadGetMethod;
exports.addUploadMultipartPostMethod = addUploadMultipartPostMethod;
exports.startApp = startApp;

var _express = _interopRequireDefault(require("express"));

var _cors = _interopRequireDefault(require("cors"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _logger = _interopRequireDefault(require("../utils/logger/logger"));

var _app_config = _interopRequireDefault(require("../config/app_config"));

var _v = _interopRequireDefault(require("uuid/v4"));

var _responseTime = _interopRequireDefault(require("response-time"));

var _multer = _interopRequireDefault(require("multer"));

var _ServiceData = _interopRequireDefault(require("../common/ServiceData"));

var _ResponseReply = _interopRequireDefault(require("../common/ResponseReply"));

var _Middleware = _interopRequireDefault(require("../utils/Middleware/Middleware"));

var _system_config = _interopRequireDefault(require("../config/system_config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var app = (0, _express.default)();
var TAG = "[Application] ";

function initial() {
  app.use((0, _cors.default)());
  app.use(_bodyParser.default.json({
    limit: '50mb'
  }));
  app.use(function (req, res, next) {
    var _systemConfig$API_DIS, _systemConfig$API_DIS2;

    // req.headers.__reqId = "[" + uuidv4() + "] ";
    // req.specialType = "request";
    req.startTime = new Date().getTime(); // stamp time start inservice

    _logger.default.info("".concat(TAG, "Call \"").concat(req.originalUrl, "\""));

    if (((_systemConfig$API_DIS = _system_config.default.API_DISABLE_LOG_HEADER) === null || _systemConfig$API_DIS === void 0 ? void 0 : _systemConfig$API_DIS.toLocaleUpperCase()) != "Y") {
      _logger.default.info("".concat(TAG, ":: Request Header :: ").concat(JSON.stringify(req.headers)));
    }

    if (((_systemConfig$API_DIS2 = _system_config.default.API_DISABLE_LOG_BODY) === null || _systemConfig$API_DIS2 === void 0 ? void 0 : _systemConfig$API_DIS2.toLocaleUpperCase()) != "Y") {
      _logger.default.info("".concat(TAG, ":: Request Data :: ").concat(JSON.stringify(req.body)));
    }

    next();
  });
  app.use((0, _responseTime.default)(function (req, res, time) {
    var totalTimeInService = time;

    if (res.statusCode == 200) {
      _logger.default.success(TAG, "RESPONSE STATUS CODE : ".concat(res.statusCode, " , MESSAGE : Ok"));
    } else {
      _logger.default.error(TAG, "RESPONSE STATUS CODE : ".concat(res.statusCode, " , MESSAGE : ").concat(res.statusMessage));
    }

    if (totalTimeInService > 100) {
      _logger.default.warning(TAG + "Total time in service [over 100ms] : ", Math.round(totalTimeInService), " ms");
    } else {
      _logger.default.info(TAG + "Total time in service : ", Math.round(totalTimeInService), " ms");
    }
  }));
  /**
   * Show App Detail
   */

  app.all("/app/version", function (req, res) {
    res.send({
      appName: _app_config.default.appName,
      description: _app_config.default.description,
      version: _app_config.default.version
    });
  });
}

function addPostMethod(uri, processor, isRequiredToken) {
  app.post(uri, _Middleware.default.VerifyToken(isRequiredToken),
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(req, res) {
      var sv, createResult;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              sv = new _ServiceData.default();
              sv.setData(req, res);
              _context.next = 5;
              return processor.process(sv);

            case 5:
              createResult = _context.sent;
              res.send(_ResponseReply.default.success(createResult));
              _context.next = 13;
              break;

            case 9:
              _context.prev = 9;
              _context.t0 = _context["catch"](0);

              _logger.default.error(_context.t0 + '');

              res.send(_ResponseReply.default.error(_context.t0));

            case 13:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 9]]);
    }));

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  }());
}

function addPostMethodBufferResponse(uri, processor, isRequiredToken) {
  app.post(uri, _Middleware.default.VerifyToken(isRequiredToken),
  /*#__PURE__*/
  function () {
    var _ref2 = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2(req, res) {
      var sv, createResult;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.prev = 0;
              sv = new _ServiceData.default();
              sv.setData(req, res);
              _context2.next = 5;
              return processor.process(sv);

            case 5:
              createResult = _context2.sent;

              if (createResult === null || createResult === void 0 ? void 0 : createResult.__httpStatus) {
                res.status(createResult === null || createResult === void 0 ? void 0 : createResult.__httpStatus).send();
              } else {
                res.end(createResult === null || createResult === void 0 ? void 0 : createResult.__buffer);
              }

              _context2.next = 13;
              break;

            case 9:
              _context2.prev = 9;
              _context2.t0 = _context2["catch"](0);

              _logger.default.error(_context2.t0 + '');

              res.status(404).send(_context2.t0);

            case 13:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[0, 9]]);
    }));

    return function (_x3, _x4) {
      return _ref2.apply(this, arguments);
    };
  }());
}

function addGetMethod(uri, processor, isRequiredToken) {
  app.get(uri, _Middleware.default.VerifyToken(isRequiredToken),
  /*#__PURE__*/
  function () {
    var _ref3 = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee3(req, res) {
      var sv, createResult;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.prev = 0;
              sv = new _ServiceData.default();
              sv.setData(req, res);
              sv.body = _objectSpread({}, req.query);
              _context3.next = 6;
              return processor.process(sv);

            case 6:
              createResult = _context3.sent;
              res.send(_ResponseReply.default.success(createResult));
              _context3.next = 14;
              break;

            case 10:
              _context3.prev = 10;
              _context3.t0 = _context3["catch"](0);

              _logger.default.error(_context3.t0 + '');

              res.send(_ResponseReply.default.error(_context3.t0));

            case 14:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, null, [[0, 10]]);
    }));

    return function (_x5, _x6) {
      return _ref3.apply(this, arguments);
    };
  }());
}

function addGetMethodBufferResponse(uri, processor, isRequiredToken) {
  app.get(uri, _Middleware.default.VerifyToken(isRequiredToken),
  /*#__PURE__*/
  function () {
    var _ref4 = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee4(req, res) {
      var sv, createResult;
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.prev = 0;
              sv = new _ServiceData.default();
              sv.setData(req, res);
              sv.body = _objectSpread({}, req.query);
              _context4.next = 6;
              return processor.process(sv);

            case 6:
              createResult = _context4.sent;

              if (createResult === null || createResult === void 0 ? void 0 : createResult.__httpStatus) {
                res.status(createResult === null || createResult === void 0 ? void 0 : createResult.__httpStatus).send();
              } else {
                res.end(createResult === null || createResult === void 0 ? void 0 : createResult.__buffer);
              }

              _context4.next = 14;
              break;

            case 10:
              _context4.prev = 10;
              _context4.t0 = _context4["catch"](0);

              _logger.default.error(_context4.t0 + '');

              res.status(404).send(_context4.t0);

            case 14:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, null, [[0, 10]]);
    }));

    return function (_x7, _x8) {
      return _ref4.apply(this, arguments);
    };
  }());
}

function addGetMethodRawResponse(uri, processor, isRequiredToken) {
  app.get(uri, _Middleware.default.VerifyToken(isRequiredToken),
  /*#__PURE__*/
  function () {
    var _ref5 = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee5(req, res) {
      var sv, createResult;
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.prev = 0;
              sv = new _ServiceData.default();
              sv.setData(req, res);
              sv.body = _objectSpread({}, req.query);
              _context5.next = 6;
              return processor.process(sv);

            case 6:
              createResult = _context5.sent;
              res.send(createResult);
              _context5.next = 14;
              break;

            case 10:
              _context5.prev = 10;
              _context5.t0 = _context5["catch"](0);

              _logger.default.error(_context5.t0 + '');

              res.send(_ResponseReply.default.error(_context5.t0));

            case 14:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, null, [[0, 10]]);
    }));

    return function (_x9, _x10) {
      return _ref5.apply(this, arguments);
    };
  }());
}

function addDownloadGetMethod(uri, processor, isRequiredToken) {
  app.get(uri, _Middleware.default.VerifyToken(isRequiredToken),
  /*#__PURE__*/
  function () {
    var _ref6 = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee6(req, res) {
      var sv, createResult;
      return regeneratorRuntime.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              _context6.prev = 0;
              sv = new _ServiceData.default();
              sv.setData(req, res);
              sv.body = _objectSpread({}, req.query);
              _context6.next = 6;
              return processor.process(sv);

            case 6:
              createResult = _context6.sent;
              res.download(createResult === null || createResult === void 0 ? void 0 : createResult.__downloadPath, function (error) {
                if (error) {
                  _logger.default.error("Error uri:".concat(uri, " =") + error);

                  res.send(_ResponseReply.default.error(error));
                }
              });
              _context6.next = 14;
              break;

            case 10:
              _context6.prev = 10;
              _context6.t0 = _context6["catch"](0);

              _logger.default.error(_context6.t0 + '');

              res.send(_ResponseReply.default.error(_context6.t0));

            case 14:
            case "end":
              return _context6.stop();
          }
        }
      }, _callee6, null, [[0, 10]]);
    }));

    return function (_x11, _x12) {
      return _ref6.apply(this, arguments);
    };
  }());
}

var upload = (0, _multer.default)({
  dest: 'uploads/'
});

function addUploadMultipartPostMethod(uri, processor, isRequiredToken, appApiUploadOptionalConfig) {
  /**
   * @type {AppApiUploadOptionalConfig}
   */
  var optionsConfig = new AppApiUploadOptionalConfig();
  optionsConfig.fileuploadKeyName = (appApiUploadOptionalConfig === null || appApiUploadOptionalConfig === void 0 ? void 0 : appApiUploadOptionalConfig.fileuploadKeyName) || 'data';
  app.post(uri, _Middleware.default.VerifyToken(isRequiredToken), upload.single(optionsConfig === null || optionsConfig === void 0 ? void 0 : optionsConfig.fileuploadKeyName),
  /*#__PURE__*/
  function () {
    var _ref7 = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee7(req, res) {
      var sv, createResult;
      return regeneratorRuntime.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              _context7.prev = 0;
              sv = new _ServiceData.default();
              sv.setData(req, res);
              _context7.next = 5;
              return processor.process(sv);

            case 5:
              createResult = _context7.sent;
              res.send(_ResponseReply.default.success(createResult));
              _context7.next = 13;
              break;

            case 9:
              _context7.prev = 9;
              _context7.t0 = _context7["catch"](0);

              _logger.default.error(_context7.t0 + '');

              res.send(_ResponseReply.default.error(_context7.t0));

            case 13:
            case "end":
              return _context7.stop();
          }
        }
      }, _callee7, null, [[0, 9]]);
    }));

    return function (_x13, _x14) {
      return _ref7.apply(this, arguments);
    };
  }());
}

function startApp(appName, version, description) {
  _app_config.default.appName = appName || _app_config.default.appName;
  _app_config.default.version = version || _app_config.default.version;
  _app_config.default.description = description || _app_config.default.description;
  var server = app.listen(_app_config.default.APP_START_PORT, function () {
    _logger.default.info("".concat(TAG, "Starting ").concat(_app_config.default.appName, " (version ").concat(_app_config.default.version, ") , Listening on port ").concat(_app_config.default.APP_START_PORT, " !"));
  }); // Set timeout for server

  server.setTimeout(1000 * 60 * 30);
}

var AppApiUploadOptionalConfig = function AppApiUploadOptionalConfig() {
  _classCallCheck(this, AppApiUploadOptionalConfig);

  this.fileuploadKeyName = "";
};