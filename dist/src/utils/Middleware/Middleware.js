"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _jwtSimple = _interopRequireDefault(require("jwt-simple"));

var _TokenUtils = _interopRequireDefault(require("../TokenUtils/TokenUtils"));

var _express = require("express");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var HEADER_TOKEN_KEY = 'token';

var Middleware =
/*#__PURE__*/
function () {
  function Middleware() {
    _classCallCheck(this, Middleware);
  }

  _createClass(Middleware, null, [{
    key: "VerifyToken",

    /**
     * @description "Middleware สำหรับตรวจสอบ Token"
     * @returns {Function}
     */
    value: function VerifyToken(isRequiredToken) {
      if (isRequiredToken && isRequiredToken == true) {
        /**
         * 
         * @param {Request} req 
         * @param {Response} res 
         * @param {*} next 
         */
        var verifyTokenMiddleWare = function verifyTokenMiddleWare(req, res, next) {
          try {
            var tokenInfo = _TokenUtils.default.decodeToken(req.headers[HEADER_TOKEN_KEY]);

            req.$token = tokenInfo;

            if ((tokenInfo === null || tokenInfo === void 0 ? void 0 : tokenInfo.create_time) && (tokenInfo === null || tokenInfo === void 0 ? void 0 : tokenInfo.expiresIn)) {
              // check expires time
              var currentTime = new Date().getTime();
              var calculatedTime = currentTime - ((tokenInfo === null || tokenInfo === void 0 ? void 0 : tokenInfo.create_time) + (tokenInfo === null || tokenInfo === void 0 ? void 0 : tokenInfo.expiresIn));

              if (calculatedTime > 0) {
                res.status(401).send("Token expired");
                return;
              } // renew token


              var renewToken = _TokenUtils.default.generateToken(_objectSpread({}, tokenInfo, {
                create_time: currentTime
              }));

              res.setHeader(HEADER_TOKEN_KEY, renewToken);
            }

            next();
          } catch (error) {
            res.status(401).send("Invalid token");
          }
        };

        return verifyTokenMiddleWare;
      } else {
        var blankMiddleWare = function blankMiddleWare(req, res, next) {
          next();
        };

        return blankMiddleWare;
      }
    }
  }]);

  return Middleware;
}();

var _default = Middleware;
exports.default = _default;