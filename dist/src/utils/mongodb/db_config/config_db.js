"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _app_config = _interopRequireDefault(require("../../../config/app_config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Config Database
var DB_USERNAME = encodeURIComponent(_app_config.default.config.DB_USERNAME);
var DB_PASSWORD = encodeURIComponent(_app_config.default.config.DB_PASSWORD);
var DB_NAME = _app_config.default.config.DB_NAME;
var DB_PORT = _app_config.default.config.DB_PORT;
var DB_URL = _app_config.default.config.DB_URL;
var DB_CONNECT_STR = "mongodb://".concat(DB_USERNAME, ":").concat(DB_PASSWORD, "@").concat(DB_URL, ":").concat(DB_PORT, "/").concat(DB_NAME);
var _default = {
  DB_CONNECT_STR: DB_CONNECT_STR,
  DB_NAME: DB_NAME
};
exports.default = _default;