"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.openConnection = openConnection;
exports.find = find;
exports.findOne = findOne;
exports.insertOne = insertOne;
exports.updateOne = updateOne;
exports.updateMany = updateMany;
exports.deleteMany = deleteMany;
exports.findOneAndUpdate = findOneAndUpdate;
exports.aggregate = aggregate;
exports.CreateSession = CreateSession;
exports.getConnectionMongoClient = getConnectionMongoClient;

var _mongodb = require("mongodb");

var _config_db = _interopRequireDefault(require("./config_db"));

var _logger = _interopRequireDefault(require("../../logger/logger"));

var _app_config = _interopRequireDefault(require("../../../config/app_config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/**
 * @type {MongoClient}
 */
var mongo = null;
var TAG = "[ConnectMongo] ";

function openConnection() {
  return _openConnection.apply(this, arguments);
}
/**
 * Select * From
 * @param {String} collectionName 
 * @param {*} queryCondition 
 * @param {Object} filter
 * @returns {Promise<Array>}
 */


function _openConnection() {
  _openConnection = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _mongodb.MongoClient.connect(_config_db.default.DB_CONNECT_STR, {
              useNewUrlParser: true,
              useUnifiedTopology: true,
              poolSize: _app_config.default.config.db_connection_pool_size
            });

          case 3:
            mongo = _context.sent;

            _logger.default.info("".concat(TAG, "::::::::::::::: Mongo DB is connected ::::::::::::::::"));

            _context.next = 10;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);

            _logger.default.error("".concat(TAG, "::::::::::::::: Cannot connect to Mongo DB ::::::::::::::::\n").concat(_context.t0));

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _openConnection.apply(this, arguments);
}

function find(_x) {
  return _find.apply(this, arguments);
}
/**
 * Select * From Where Only One Data
 * @param {String} collectionName 
 * @param {*} queryCondition
 * @param {Object} filter 
 */


function _find() {
  _find = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(collectionName) {
    var queryCondition,
        filter,
        dbo,
        resultObject,
        _args2 = arguments;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            queryCondition = _args2.length > 1 && _args2[1] !== undefined ? _args2[1] : {};
            filter = _args2.length > 2 && _args2[2] !== undefined ? _args2[2] : {};
            _context2.prev = 2;

            if (!(mongo === null || !mongo.isConnected())) {
              _context2.next = 6;
              break;
            }

            _context2.next = 6;
            return openConnection();

          case 6:
            dbo = mongo.db(_config_db.default.DB_NAME);
            _context2.next = 9;
            return dbo.collection(collectionName).find(queryCondition, {
              projection: _objectSpread({}, filter)
            }).toArray();

          case 9:
            resultObject = _context2.sent;

            _logger.default.db("Find data from " + collectionName + " Success.");

            return _context2.abrupt("return", resultObject);

          case 14:
            _context2.prev = 14;
            _context2.t0 = _context2["catch"](2);
            throw _context2.t0;

          case 17:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[2, 14]]);
  }));
  return _find.apply(this, arguments);
}

function findOne(_x2) {
  return _findOne.apply(this, arguments);
}
/**
 * Insert One Data
 * @param {String} collectionName 
 * @param {OptionalId<Object>} data 
 * @param {CollectionInsertOneOptions} options
 */


function _findOne() {
  _findOne = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3(collectionName) {
    var queryCondition,
        filter,
        dbo,
        resultObject,
        _args3 = arguments;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            queryCondition = _args3.length > 1 && _args3[1] !== undefined ? _args3[1] : {};
            filter = _args3.length > 2 && _args3[2] !== undefined ? _args3[2] : {};
            _context3.prev = 2;

            if (!(mongo === null || !mongo.isConnected())) {
              _context3.next = 6;
              break;
            }

            _context3.next = 6;
            return openConnection();

          case 6:
            dbo = mongo.db(_config_db.default.DB_NAME);
            _context3.next = 9;
            return dbo.collection(collectionName).findOne(queryCondition, {
              projection: _objectSpread({}, filter)
            });

          case 9:
            resultObject = _context3.sent;

            _logger.default.db("Find data from " + collectionName + " Success.");

            return _context3.abrupt("return", resultObject);

          case 14:
            _context3.prev = 14;
            _context3.t0 = _context3["catch"](2);
            throw _context3.t0;

          case 17:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[2, 14]]);
  }));
  return _findOne.apply(this, arguments);
}

function insertOne(_x3) {
  return _insertOne.apply(this, arguments);
}
/**
 * Update Data
 * @param {String} collectionName 
 * @param {FilterQuery<Object>} queryCondition 
 * @param {*} updateValue 
 * @param {UpdateOneOptions} options
 * @return {UpdateWriteOpResult}
 */


function _insertOne() {
  _insertOne = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee4(collectionName) {
    var data,
        options,
        dbo,
        resultObject,
        _args4 = arguments;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            data = _args4.length > 1 && _args4[1] !== undefined ? _args4[1] : {};
            options = _args4.length > 2 && _args4[2] !== undefined ? _args4[2] : undefined;
            _context4.prev = 2;

            if (!(mongo === null || !mongo.isConnected())) {
              _context4.next = 6;
              break;
            }

            _context4.next = 6;
            return openConnection();

          case 6:
            dbo = mongo.db(_config_db.default.DB_NAME);
            _context4.next = 9;
            return dbo.collection(collectionName).insertOne(data, options);

          case 9:
            resultObject = _context4.sent;

            _logger.default.db("Insert data to " + collectionName + " Success.");

            return _context4.abrupt("return", resultObject);

          case 14:
            _context4.prev = 14;
            _context4.t0 = _context4["catch"](2);
            throw _context4.t0;

          case 17:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[2, 14]]);
  }));
  return _insertOne.apply(this, arguments);
}

function updateOne(_x4) {
  return _updateOne.apply(this, arguments);
}
/**
 * Update Many Data
 * @param {String} collectionName 
 * @param {*} queryCondition 
 * @param {*} updateValue 
 * @param {UpdateManyOptions} options
 */


function _updateOne() {
  _updateOne = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee5(collectionName) {
    var queryCondition,
        updateValue,
        options,
        dbo,
        resultObject,
        _args5 = arguments;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            queryCondition = _args5.length > 1 && _args5[1] !== undefined ? _args5[1] : {};
            updateValue = _args5.length > 2 && _args5[2] !== undefined ? _args5[2] : {};
            options = _args5.length > 3 && _args5[3] !== undefined ? _args5[3] : undefined;
            _context5.prev = 3;

            if (!(mongo === null || !mongo.isConnected())) {
              _context5.next = 7;
              break;
            }

            _context5.next = 7;
            return openConnection();

          case 7:
            dbo = mongo.db(_config_db.default.DB_NAME);
            _context5.next = 10;
            return dbo.collection(collectionName).updateOne(queryCondition, updateValue, options);

          case 10:
            resultObject = _context5.sent;

            _logger.default.db("Update data to " + collectionName + " Success.");

            return _context5.abrupt("return", resultObject);

          case 15:
            _context5.prev = 15;
            _context5.t0 = _context5["catch"](3);
            throw _context5.t0;

          case 18:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[3, 15]]);
  }));
  return _updateOne.apply(this, arguments);
}

function updateMany(_x5) {
  return _updateMany.apply(this, arguments);
}
/**
 * Delete From
 * @param {String} collectionName 
 * @param {*} queryCondition 
 * @return {DeleteWriteOpResultObject}
 */


function _updateMany() {
  _updateMany = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee6(collectionName) {
    var queryCondition,
        updateValue,
        options,
        dbo,
        resultObject,
        _args6 = arguments;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            queryCondition = _args6.length > 1 && _args6[1] !== undefined ? _args6[1] : {};
            updateValue = _args6.length > 2 && _args6[2] !== undefined ? _args6[2] : {};
            options = _args6.length > 3 && _args6[3] !== undefined ? _args6[3] : undefined;
            _context6.prev = 3;

            if (!(mongo === null || !mongo.isConnected())) {
              _context6.next = 7;
              break;
            }

            _context6.next = 7;
            return openConnection();

          case 7:
            dbo = mongo.db(_config_db.default.DB_NAME);
            _context6.next = 10;
            return dbo.collection(collectionName).updateMany(queryCondition, updateValue, options);

          case 10:
            resultObject = _context6.sent;

            _logger.default.db("Update data to " + collectionName + " Success.");

            return _context6.abrupt("return", resultObject);

          case 15:
            _context6.prev = 15;
            _context6.t0 = _context6["catch"](3);
            throw _context6.t0;

          case 18:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[3, 15]]);
  }));
  return _updateMany.apply(this, arguments);
}

function deleteMany(_x6, _x7) {
  return _deleteMany.apply(this, arguments);
}
/**
* @description Find a document and update it in one atomic operation. Requires a write lock for the duration of the operation.
* http://mongodb.github.io/node-mongodb-native/3.3/api/Collection.html#findOneAndUpdate
* @param {String} collectionName 
* @param {FilterQuery<Object>} queryCondition 
* @param {UpdateQuery<Object>} updateValue 
* @param {FindOneAndUpdateOption} options
* 
* @returns {FindAndModifyWriteOpResultObject<Object>}
*/


function _deleteMany() {
  _deleteMany = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee7(collectionName, queryCondition) {
    var dbo, resultObject;
    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;

            if (!(mongo === null || !mongo.isConnected())) {
              _context7.next = 4;
              break;
            }

            _context7.next = 4;
            return openConnection();

          case 4:
            dbo = mongo.db(_config_db.default.DB_NAME);
            _context7.next = 7;
            return dbo.collection(collectionName).deleteMany(queryCondition);

          case 7:
            resultObject = _context7.sent;

            _logger.default.db("Delete data from " + collectionName + " Success.");

            return _context7.abrupt("return", resultObject);

          case 12:
            _context7.prev = 12;
            _context7.t0 = _context7["catch"](0);
            throw _context7.t0;

          case 15:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 12]]);
  }));
  return _deleteMany.apply(this, arguments);
}

function findOneAndUpdate(_x8) {
  return _findOneAndUpdate.apply(this, arguments);
}
/**
 * Select * From
 * @param {String} collectionName 
 * @param {Object[]} pipeline 
 */


function _findOneAndUpdate() {
  _findOneAndUpdate = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee8(collectionName) {
    var queryCondition,
        updateValue,
        options,
        dbo,
        resultObject,
        _args8 = arguments;
    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            queryCondition = _args8.length > 1 && _args8[1] !== undefined ? _args8[1] : {};
            updateValue = _args8.length > 2 && _args8[2] !== undefined ? _args8[2] : {};
            options = _args8.length > 3 && _args8[3] !== undefined ? _args8[3] : undefined;
            _context8.prev = 3;

            if (!(mongo === null || !mongo.isConnected())) {
              _context8.next = 7;
              break;
            }

            _context8.next = 7;
            return openConnection();

          case 7:
            dbo = mongo.db(_config_db.default.DB_NAME);
            _context8.next = 10;
            return dbo.collection(collectionName).findOneAndUpdate(queryCondition, updateValue, options);

          case 10:
            resultObject = _context8.sent;

            _logger.default.db("Update data to " + collectionName + " Success.");

            return _context8.abrupt("return", resultObject);

          case 15:
            _context8.prev = 15;
            _context8.t0 = _context8["catch"](3);
            throw _context8.t0;

          case 18:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[3, 15]]);
  }));
  return _findOneAndUpdate.apply(this, arguments);
}

function aggregate(_x9) {
  return _aggregate.apply(this, arguments);
}
/**
 * @returns {ClientSession}
 */


function _aggregate() {
  _aggregate = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee9(collectionName) {
    var pipeline,
        dbo,
        resultObject,
        _args9 = arguments;
    return regeneratorRuntime.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            pipeline = _args9.length > 1 && _args9[1] !== undefined ? _args9[1] : {};
            _context9.prev = 1;

            if (!(mongo === null || !mongo.isConnected())) {
              _context9.next = 5;
              break;
            }

            _context9.next = 5;
            return openConnection();

          case 5:
            dbo = mongo.db(_config_db.default.DB_NAME);
            _context9.next = 8;
            return dbo.collection(collectionName).aggregate(pipeline).toArray();

          case 8:
            resultObject = _context9.sent;

            _logger.default.db("Find data from " + collectionName + " Success.");

            return _context9.abrupt("return", resultObject);

          case 13:
            _context9.prev = 13;
            _context9.t0 = _context9["catch"](1);
            throw _context9.t0;

          case 16:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9, null, [[1, 13]]);
  }));
  return _aggregate.apply(this, arguments);
}

function CreateSession() {
  return _CreateSession.apply(this, arguments);
}
/**
 * @returns {MongoClient}
 */


function _CreateSession() {
  _CreateSession = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee10() {
    var sessions;
    return regeneratorRuntime.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.prev = 0;

            if (!(mongo === null || !mongo.isConnected())) {
              _context10.next = 4;
              break;
            }

            _context10.next = 4;
            return openConnection();

          case 4:
            sessions = mongo.startSession({
              defaultTransactionOptions: {
                readConcern: {
                  level: 'local'
                },
                writeConcern: {
                  w: 'majority'
                },
                readPreference: 'primary'
              }
            });
            return _context10.abrupt("return", sessions);

          case 8:
            _context10.prev = 8;
            _context10.t0 = _context10["catch"](0);
            throw _context10.t0;

          case 11:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10, null, [[0, 8]]);
  }));
  return _CreateSession.apply(this, arguments);
}

function getConnectionMongoClient() {
  return _getConnectionMongoClient.apply(this, arguments);
}

function _getConnectionMongoClient() {
  _getConnectionMongoClient = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee11() {
    return regeneratorRuntime.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.prev = 0;

            if (!(mongo === null || !mongo.isConnected())) {
              _context11.next = 4;
              break;
            }

            _context11.next = 4;
            return openConnection();

          case 4:
            return _context11.abrupt("return", mongo);

          case 7:
            _context11.prev = 7;
            _context11.t0 = _context11["catch"](0);
            throw _context11.t0;

          case 10:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11, null, [[0, 7]]);
  }));
  return _getConnectionMongoClient.apply(this, arguments);
}