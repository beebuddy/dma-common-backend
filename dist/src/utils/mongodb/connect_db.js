"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _collections = _interopRequireDefault(require("./collections"));

var dbActions = _interopRequireWildcard(require("./db_config/action"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var _ConnectMongo =
/*#__PURE__*/
function () {
  function _ConnectMongo() {
    _classCallCheck(this, _ConnectMongo);
  }

  _createClass(_ConnectMongo, [{
    key: "initial",
    value: function initial() {
      dbActions.openConnection();
    }
  }, {
    key: "getDB",
    value: function getDB() {
      return dbActions;
    }
  }]);

  return _ConnectMongo;
}();

var ConnectMongo = new _ConnectMongo();
var _default = ConnectMongo;
exports.default = _default;