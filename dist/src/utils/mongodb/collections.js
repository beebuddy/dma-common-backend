"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  DMA5_DMA_OBJECT: "dma5_dma_object",
  DMA5_CONFIG: "dma5_config",
  DMA5_BUILDER_ENV: "dma5_builder_env",
  DMA5_PUBLISH_QUEUE: "dma5_publish_queue",
  DMA5_CARD_PROTOTYPE: "dma5_card_prototype",
  DMA_SCREEN: "app_ui_config",
  DMA_STORYBOARD: "story_board_config",
  DMA_STORYBOARD_LIST: "storyboard_storyboardlist",
  DMA_STORYBOARD_GROUP: "storyboard_groups",
  DMA_MASTER_NATIVE_INPUT: 'screen_native_input_type'
};
exports.default = _default;