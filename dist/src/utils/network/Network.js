"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _fsExtra = _interopRequireDefault(require("fs-extra"));

var _axios = require("axios");

var _CallAPI = _interopRequireDefault(require("./CallAPI"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Network =
/*#__PURE__*/
function () {
  function Network() {
    _classCallCheck(this, Network);
  }

  _createClass(Network, null, [{
    key: "requestPost",

    /**
     * 
     * @param {String} url 
     * @param {*} params 
     * @param {AxiosRequestConfig} config 
     */
    value: function () {
      var _requestPost = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(url) {
        var params,
            config,
            mergeConfig,
            response,
            status,
            data,
            message,
            _args = arguments;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                params = _args.length > 1 && _args[1] !== undefined ? _args[1] : {};
                config = _args.length > 2 && _args[2] !== undefined ? _args[2] : {};
                _context.prev = 2;
                mergeConfig = _objectSpread({
                  // default timeout
                  timeout: 30 * 60 * 1000,
                  // 30 minutes
                  maxContentLength: Infinity,
                  maxBodyLength: Infinity
                }, config);
                _context.next = 6;
                return _CallAPI.default.post(url, params, mergeConfig);

              case 6:
                response = _context.sent;
                return _context.abrupt("return", response.data);

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](2);
                status = _context.t0.status, data = _context.t0.data, message = _context.t0.message;

                if (!(status === 409)) {
                  _context.next = 17;
                  break;
                }

                throw data;

              case 17:
                throw message ? message : _context.t0;

              case 18:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[2, 10]]);
      }));

      return function requestPost(_x) {
        return _requestPost.apply(this, arguments);
      };
    }()
    /**
     * 
     * @param {String} urlWithParams 
     * @param {AxiosRequestConfig} config 
     */

  }, {
    key: "requestGet",
    value: function () {
      var _requestGet = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(urlWithParams) {
        var config,
            mergeConfig,
            response,
            status,
            data,
            message,
            _args2 = arguments;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                config = _args2.length > 1 && _args2[1] !== undefined ? _args2[1] : {};
                _context2.prev = 1;
                mergeConfig = _objectSpread({
                  // default timeout
                  timeout: 30 * 60 * 1000,
                  // 30 minutes
                  maxContentLength: Infinity,
                  maxBodyLength: Infinity
                }, config);
                _context2.next = 5;
                return _CallAPI.default.get(urlWithParams, mergeConfig);

              case 5:
                response = _context2.sent;
                return _context2.abrupt("return", response.data);

              case 9:
                _context2.prev = 9;
                _context2.t0 = _context2["catch"](1);
                status = _context2.t0.status, data = _context2.t0.data, message = _context2.t0.message;

                if (!(status === 409)) {
                  _context2.next = 16;
                  break;
                }

                throw data;

              case 16:
                throw message ? message : _context2.t0;

              case 17:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[1, 9]]);
      }));

      return function requestGet(_x2) {
        return _requestGet.apply(this, arguments);
      };
    }()
    /**
     * 
     * @param {String} url 
     * @param {String} fileOutput
     * @param {*} params 
     * @param {AxiosRequestConfig} config 
     */

  }, {
    key: "requestDownloadPost",
    value: function () {
      var _requestDownloadPost = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3(url, fileOutput) {
        var params,
            config,
            mergeConfig,
            resultDownload,
            downloadFileData,
            _args3 = arguments;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                params = _args3.length > 2 && _args3[2] !== undefined ? _args3[2] : {};
                config = _args3.length > 3 && _args3[3] !== undefined ? _args3[3] : {};
                _context3.prev = 2;
                mergeConfig = _objectSpread({
                  responseType: "stream",
                  maxContentLength: Infinity,
                  maxBodyLength: Infinity
                }, config);
                _context3.next = 6;
                return this.requestPost(url, params, mergeConfig);

              case 6:
                resultDownload = _context3.sent;
                downloadFileData = _fsExtra.default.createWriteStream(fileOutput);
                _context3.next = 10;
                return new Promise(function (resolve, reject) {
                  resultDownload.pipe(downloadFileData);
                  var error = null;
                  downloadFileData.on("error", function (error) {
                    error = err;
                    downloadFileData.close();
                    reject(error);
                  });
                  downloadFileData.on('close', function () {
                    if (!error) {
                      resolve(true);
                    }
                  });
                });

              case 10:
                return _context3.abrupt("return", true);

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3["catch"](2);
                throw _context3.t0;

              case 16:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[2, 13]]);
      }));

      return function requestDownloadPost(_x3, _x4) {
        return _requestDownloadPost.apply(this, arguments);
      };
    }()
    /**
     * 
     * @param {String} url 
     * @param {String} fileOutput 
     * @param {AxiosRequestConfig} config 
     */

  }, {
    key: "requestDownloadGet",
    value: function () {
      var _requestDownloadGet = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4(url, fileOutput) {
        var config,
            mergeConfig,
            resultDownload,
            downloadFileData,
            _args4 = arguments;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                config = _args4.length > 2 && _args4[2] !== undefined ? _args4[2] : {};
                _context4.prev = 1;
                mergeConfig = _objectSpread({
                  responseType: "stream",
                  maxContentLength: Infinity,
                  maxBodyLength: Infinity
                }, config);
                _context4.next = 5;
                return this.requestGet(url, mergeConfig);

              case 5:
                resultDownload = _context4.sent;
                downloadFileData = _fsExtra.default.createWriteStream(fileOutput);
                _context4.next = 9;
                return new Promise(function (resolve, reject) {
                  resultDownload.pipe(downloadFileData);
                  var error = null;
                  downloadFileData.on("error", function (error) {
                    error = err;
                    downloadFileData.close();
                    reject(error);
                  });
                  downloadFileData.on('close', function () {
                    if (!error) {
                      resolve(true);
                    }
                  });
                });

              case 9:
                return _context4.abrupt("return", true);

              case 12:
                _context4.prev = 12;
                _context4.t0 = _context4["catch"](1);
                throw _context4.t0;

              case 15:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[1, 12]]);
      }));

      return function requestDownloadGet(_x5, _x6) {
        return _requestDownloadGet.apply(this, arguments);
      };
    }()
  }]);

  return Network;
}();

exports.default = Network;