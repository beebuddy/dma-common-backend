"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _axios = _interopRequireWildcard(require("axios"));

var _logger = _interopRequireDefault(require("../logger/logger"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var TAG = "[Network]";

var CallAPI =
/*#__PURE__*/
function () {
  function CallAPI() {
    _classCallCheck(this, CallAPI);
  }

  _createClass(CallAPI, null, [{
    key: "post",

    /**
     * @param {string} url 
     * @param {JSON} body 
     * @param {AxiosRequestConfig} customConfig
     * @returns {AxiosPromise}
     */
    value: function () {
      var _post = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(url, body) {
        var customConfig,
            config,
            response,
            myError,
            _args = arguments;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                customConfig = _args.length > 2 && _args[2] !== undefined ? _args[2] : null;
                _context.prev = 1;

                /**
                * @type {customConfig}
                */
                config = this.getNetworkConfig();

                if (customConfig != null) {
                  config = _objectSpread({}, config, customConfig);
                }

                _context.next = 6;
                return _axios.default.post(url, body, config);

              case 6:
                response = _context.sent;

                if (response.status === 200) {
                  _logger.default.success("".concat(TAG, " Endpoint url = ").concat(url, " , STATUS CODE : ").concat(response.status, " , Message : OK"));
                } else {
                  _logger.default.error("".concat(TAG, " Endpoint url = ").concat(url, " , STATUS CODE : ").concat(response.status, " , Message : ").concat(response.statusText));
                }

                return _context.abrupt("return", response);

              case 11:
                _context.prev = 11;
                _context.t0 = _context["catch"](1);
                myError = {
                  message: _context.t0.message,
                  status: _context.t0.response ? _context.t0.response.status : "",
                  data: _context.t0.response ? _context.t0.response.data : "",
                  response: {
                    headers: _context.t0.response ? _context.t0.response.headers : ""
                  },
                  errorConfig: _context.t0.config
                };

                _logger.default.error("".concat(TAG, " Endpoint url = ").concat(url, " , STATUS CODE : ").concat(myError.status, " , Message : ").concat("" + myError.message));

                throw myError;

              case 16:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[1, 11]]);
      }));

      return function post(_x, _x2) {
        return _post.apply(this, arguments);
      };
    }()
    /**
     * 
     * @param {String} urlWithParam 
     * @param {AxiosRequestConfig} config 
     */

  }, {
    key: "get",
    value: function () {
      var _get = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(urlWithParam) {
        var config,
            response,
            myError,
            _args2 = arguments;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                config = _args2.length > 1 && _args2[1] !== undefined ? _args2[1] : null;
                _context2.prev = 1;
                _context2.next = 4;
                return _axios.default.get(urlWithParam, config);

              case 4:
                response = _context2.sent;

                if (response.status === 200) {
                  _logger.default.success("".concat(TAG, " Endpoint url = ").concat(urlWithParam, " , STATUS CODE : ").concat(response.status, " , Message : OK"));
                } else {
                  _logger.default.error("".concat(TAG, " Endpoint url = ").concat(urlWithParam, " , STATUS CODE : ").concat(response.status, " , Message : ").concat(response.statusText));
                }

                return _context2.abrupt("return", response);

              case 9:
                _context2.prev = 9;
                _context2.t0 = _context2["catch"](1);
                myError = {
                  message: _context2.t0.message,
                  status: _context2.t0.response ? _context2.t0.response.status : "",
                  data: _context2.t0.response ? _context2.t0.response.data : "",
                  response: {
                    headers: _context2.t0.response ? _context2.t0.response.headers : ""
                  },
                  errorConfig: _context2.t0.config
                };

                _logger.default.error("".concat(TAG, " Endpoint url = ").concat(urlWithParam, " , STATUS CODE : ").concat(myError.status, " , Message : ").concat("" + myError.message));

                throw myError;

              case 14:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[1, 9]]);
      }));

      return function get(_x3) {
        return _get.apply(this, arguments);
      };
    }()
    /**
       * @returns {AxiosRequestConfig}
       */

  }, {
    key: "getNetworkConfig",
    value: function getNetworkConfig() {
      /**
      * @type {AxiosRequestConfig}
       */
      var config = {
        timeout: 1000 * 60 * 2
      };
      return config;
    }
  }]);

  return CallAPI;
}();

var _default = CallAPI;
exports.default = _default;