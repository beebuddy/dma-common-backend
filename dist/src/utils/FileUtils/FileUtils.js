"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _fsExtra = _interopRequireDefault(require("fs-extra"));

var _ServiceData = _interopRequireDefault(require("../../common/ServiceData"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var FileUtils =
/*#__PURE__*/
function () {
  function FileUtils() {
    _classCallCheck(this, FileUtils);
  }

  _createClass(FileUtils, null, [{
    key: "getUploadFile",

    /**
     * 
     * @param {ServiceData} sv
     * @returns {MulterStructure} 
     */
    value: function getUploadFile(sv) {
      var _sv$req;

      /**
       * @type {MulterStructure}
       */
      var uploadFile = (_sv$req = sv.req) === null || _sv$req === void 0 ? void 0 : _sv$req.file;
      return uploadFile;
    }
  }]);

  return FileUtils;
}();

exports.default = FileUtils;