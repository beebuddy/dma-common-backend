"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _v = _interopRequireDefault(require("uuid/v4"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var GenUtils =
/*#__PURE__*/
function () {
  function GenUtils() {
    _classCallCheck(this, GenUtils);
  }

  _createClass(GenUtils, null, [{
    key: "genUUIDV4",

    /**
     * @description Ex. 7f1ab2d0-4575-4b01-92e1-486b977a0570
     * @returns {String}
     */
    value: function genUUIDV4() {
      return (0, _v.default)();
    }
    /**
     * @description Ex. c49a9c513a29477abf0632e8b66385b4
     * @returns {String}
     */

  }, {
    key: "genCleanUUIDV4",
    value: function genCleanUUIDV4() {
      /**
       * @type {String}
       */
      var uuid = (0, _v.default)();
      uuid = uuid.replace(/-/g, "");
      return uuid;
    }
  }]);

  return GenUtils;
}();

var _default = GenUtils;
exports.default = _default;