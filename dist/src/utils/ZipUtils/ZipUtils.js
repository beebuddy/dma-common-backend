"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _os = _interopRequireDefault(require("os"));

var _shelljs = _interopRequireDefault(require("shelljs"));

var _fsExtra = _interopRequireDefault(require("fs-extra"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ZipUtils =
/*#__PURE__*/
function () {
  function ZipUtils() {
    _classCallCheck(this, ZipUtils);
  }

  _createClass(ZipUtils, null, [{
    key: "zip",

    /**
     * @description ZipUtils.zip(`/xx/floder`,`/output/myzip.zip`)
     * @param {String} sourceFloderPath 
     * @param {String} destinationWithFieName 
     * @param {ZipUtils_ZipOptions} options
     * @returns {String}
     */
    value: function () {
      var _zip2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(sourceFloderPath, destinationWithFieName, options) {
        var splitSourcePath, sourceDir, splitDestinationWithFieName, fileName, outputDir;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                checkOSSupport();
                splitSourcePath = sourceFloderPath.split("/");

                if (!(splitSourcePath.length <= 1)) {
                  _context.next = 4;
                  break;
                }

                throw "You can't zip from root path";

              case 4:
                sourceDir = splitSourcePath[splitSourcePath.length - 1];
                splitDestinationWithFieName = destinationWithFieName.split("/");

                if (!(splitDestinationWithFieName.length <= 0)) {
                  _context.next = 8;
                  break;
                }

                throw "Invalid destinationWithFieName";

              case 8:
                fileName = splitDestinationWithFieName[splitDestinationWithFieName.length - 1];
                outputDir = destinationWithFieName.replace(fileName, ''); // check has password

                if (!(options === null || options === void 0 ? void 0 : options.password)) {
                  _context.next = 14;
                  break;
                }

                return _context.abrupt("return", this._zipWithPassword(sourceFloderPath, outputDir, fileName, options === null || options === void 0 ? void 0 : options.password));

              case 14:
                return _context.abrupt("return", this._zip(sourceFloderPath, sourceDir, outputDir, fileName));

              case 15:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function zip(_x, _x2, _x3) {
        return _zip2.apply(this, arguments);
      };
    }()
    /**
     * @description ZipUtils.zip('/xx/floder',floder,'/output/','myzip.zip')
     * @param {String} sourcePath 
     * @param {String} fromFloderName
     * @param {String} destinationFolder 
     * @param {String} zipFileName 
     * @returns {String}
     */

  }, {
    key: "_zip",
    value: function () {
      var _zip3 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(sourcePath, fromFloderName, destinationFolder, zipFileName) {
        var zipFilePath, exec_result;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                zipFilePath = destinationFolder + zipFileName;

                _fsExtra.default.ensureDirSync(destinationFolder);

                exec_result = _shelljs.default.exec("cd ".concat(sourcePath, " && zip -r ").concat(zipFilePath, " ."));

                if (!(exec_result.stderr.length > 0)) {
                  _context2.next = 5;
                  break;
                }

                throw exec_result.stderr;

              case 5:
                return _context2.abrupt("return", zipFilePath);

              case 6:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function _zip(_x4, _x5, _x6, _x7) {
        return _zip3.apply(this, arguments);
      };
    }()
    /**
     * 
     * @param {String} sourcePath 
     * @param {String} destinationFolder 
     * @param {String} zipFileName 
     * @param {String} password 
     * @returns {String}
     */

  }, {
    key: "_zipWithPassword",
    value: function () {
      var _zipWithPassword2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3(sourcePath, destinationFolder, zipFileName, password) {
        var zipFilePath, exec_result;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                zipFilePath = destinationFolder + zipFileName;

                _fsExtra.default.ensureDirSync(destinationFolder);

                exec_result = _shelljs.default.exec("cd ".concat(sourcePath, " && zip --password ").concat(password, " -r ").concat(zipFilePath, " ."));

                if (!(exec_result.stderr.length > 0)) {
                  _context3.next = 5;
                  break;
                }

                throw exec_result.stderr;

              case 5:
                return _context3.abrupt("return", zipFilePath);

              case 6:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      return function _zipWithPassword(_x8, _x9, _x10, _x11) {
        return _zipWithPassword2.apply(this, arguments);
      };
    }()
    /**
     * @description ZipUtils.unzip("/tmp/myzip.zip",/tmp/output)
     * @param {String} zipFilePath 
     * @param {String} extractToPath 
     * @param {ZipUtils_ZipOptions} options
     * @returns {String}
     */

  }, {
    key: "unzip",
    value: function () {
      var _unzip = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4(zipFilePath, extractToPath, options) {
        var exec_result;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                checkOSSupport();
                exec_result = "";

                if (options === null || options === void 0 ? void 0 : options.password) {
                  exec_result = _shelljs.default.exec("unzip -o -P ".concat(options.password, " ").concat(zipFilePath, " -d ").concat(extractToPath));
                } else {
                  exec_result = _shelljs.default.exec("unzip -o ".concat(zipFilePath, " -d ").concat(extractToPath));
                }

                if (!(exec_result.stderr.length > 0)) {
                  _context4.next = 5;
                  break;
                }

                throw exec_result.stderr;

              case 5:
                return _context4.abrupt("return", extractToPath);

              case 6:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      return function unzip(_x12, _x13, _x14) {
        return _unzip.apply(this, arguments);
      };
    }()
  }]);

  return ZipUtils;
}();

var _default = ZipUtils;
exports.default = _default;

var checkOSSupport = function checkOSSupport() {
  if (_os.default.platform() === "win32") {
    throw "Unsupported module";
  }
};

var ZipUtils_ZipOptions = function ZipUtils_ZipOptions() {
  _classCallCheck(this, ZipUtils_ZipOptions);

  this.password = "";
};