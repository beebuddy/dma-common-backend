"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TokenInfo = exports.default = void 0;

var _jwtSimple = _interopRequireDefault(require("jwt-simple"));

var _app_config = _interopRequireDefault(require("../../config/app_config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var secretKey = _app_config.default.config.TOKEN_SECRET_KEY || "!qAzxcv,!@#x";

var TokenUtils =
/*#__PURE__*/
function () {
  function TokenUtils() {
    _classCallCheck(this, TokenUtils);
  }

  _createClass(TokenUtils, null, [{
    key: "generateToken",

    /**
     * @param {TokenInfo} payload 
     * @param {String} yourSecretKey
     */
    value: function generateToken(payload) {
      var yourSecretKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      return _jwtSimple.default.encode(payload, yourSecretKey || secretKey);
    }
    /**
     * @param {String} token
     * @param {String} yourSecretKey
     * @returns {TokenInfo}
     */

  }, {
    key: "decodeToken",
    value: function decodeToken(token) {
      var yourSecretKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      return _jwtSimple.default.decode(token, yourSecretKey || secretKey);
    }
  }]);

  return TokenUtils;
}();

exports.default = TokenUtils;

var TokenInfo = function TokenInfo() {
  _classCallCheck(this, TokenInfo);

  this.username = "";
  /**
   * @type {'admin' | 'user'}
   */

  this.role = "";
  this.create_time = 1578108112;
  this.data_access_expiration_time = 1578108112;
  this.expiresIn = 5888;
  this.identityFacebook = "";
  this.data = {};
};

exports.TokenInfo = TokenInfo;