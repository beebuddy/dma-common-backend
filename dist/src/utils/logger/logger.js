"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  log: function log() {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var req = args[0];
    console.log("\x1B[0m".concat(toString(args).join(''), "\x1B[0m"));
  },
  info: function info() {
    for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    var req = args[0];
    console.log("\x1B[36m".concat(toString(args).join(''), "\x1B[0m"));
  },
  error: function error() {
    for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      args[_key3] = arguments[_key3];
    }

    var req = args[0];
    console.log("\x1B[31m".concat(toString(args).join(''), "\x1B[0m"));
  },
  warning: function warning() {
    for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
      args[_key4] = arguments[_key4];
    }

    var req = args[0];
    console.log("\x1B[33m".concat(toString(args).join(''), "\x1B[0m"));
  },
  success: function success() {
    for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
      args[_key5] = arguments[_key5];
    }

    var req = args[0];
    console.log("\x1B[32m".concat(toString(args).join(''), "\x1B[0m"));
  },
  magenta: function magenta() {
    for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
      args[_key6] = arguments[_key6];
    }

    var req = args[0];
    console.log("\x1B[35m".concat(toString(args).join(''), "\x1B[0m"));
  },
  random: function random() {
    for (var _len7 = arguments.length, args = new Array(_len7), _key7 = 0; _key7 < _len7; _key7++) {
      args[_key7] = arguments[_key7];
    }

    var req = args[0];
    console.log(toString(args).join('').split('').map(function (item) {
      return '\x1b[' + randomColor() + 'm' + item + '\x1b[0m';
    }).join('') + '\n');
  },
  db: function db() {
    for (var _len8 = arguments.length, args = new Array(_len8), _key8 = 0; _key8 < _len8; _key8++) {
      args[_key8] = arguments[_key8];
    }

    var req = args[0];
    console.log("\x1B[35m[Database Message] ".concat(toString(args).join(''), "\x1B[0m"));
  }
  /**
   * Random color not duplicate next character
   */

};
exports.default = _default;
var tempColor = null;

var randomColor = function randomColor() {
  if (tempColor != null) {
    var rand = Math.floor(Math.random() * (37 - 31) + 31);

    if (rand != tempColor) {
      tempColor = rand;
      return rand;
    } else {
      return randomColor();
    }
  } else {
    tempColor = Math.floor(Math.random() * (37 - 31) + 31);
    return tempColor;
  }
};

var toString = function toString(list) {
  return list.map(function (item) {
    return item.constructor.name == 'Object' || item.constructor.name == 'Array' ? JSON.stringify(item) : item;
  });
};