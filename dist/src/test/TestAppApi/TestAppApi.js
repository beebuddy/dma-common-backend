"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = TestAppApi;

var _ = require("../..");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function TestAppApi() {
  return _TestAppApi.apply(this, arguments);
}

function _TestAppApi() {
  _TestAppApi = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            console.log("ENV = ", _.ENV); // let token = TokenUtils.generateToken({
            //     username : "ter"
            // })
            // console.log("token = ",token)
            //console.log("decode =",TokenUtils.decodeToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InRlciJ9.oIc1zOgBaxJguDgX3FPlvs7Y_aaBQhLQvmWwRZGbtNA"))
            // intitial app middleware

            _.AppApi.initial();

            _.AppApi.addGetMethod("/test1", {
              process: function process(sv) {
                console.log("It' working", sv.body);
              }
            }, true);

            _.AppApi.addGetMethodRawResponse("/fake", {
              process: function process(sv) {
                console.log("It' working", sv.body);
                return {
                  "raw": {
                    x: "1",
                    body: sv.body
                  }
                };
              }
            });

            _.AppApi.addDownloadGetMethod("/test2", {
              process: function process(sv) {
                console.log("It' working", sv.body);
                return {
                  __downloadPath: "/Users/tanawat/Downloads/IMG_8611.PNG"
                };
              }
            });

            _.AppApi.startApp("test", "xxx");

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _TestAppApi.apply(this, arguments);
}