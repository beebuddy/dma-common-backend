"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MulterStructure = exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ServiceData =
/*#__PURE__*/
function () {
  function ServiceData() {
    _classCallCheck(this, ServiceData);
  }
  /**
   * 
   * @param {*} req 
   * @param {*} res 
   */


  _createClass(ServiceData, [{
    key: "setData",
    value: function setData(req, res) {
      this.body = req.body;
      this.headers = req.headers;
      this.req = req;
      this.res = res;
      this.token = req.$token;
    }
  }]);

  return ServiceData;
}();

exports.default = ServiceData;

var MulterStructure = function MulterStructure() {
  _classCallCheck(this, MulterStructure);

  /**
   * Field name specified in the form
   * @example 'template'
   */
  this.fieldname = "";
  /**
   * Name of the file on the user's computer
   * @example 'Desktop.zip'
   */

  this.originalname = "";
  /**
   * Encoding type of the file
   * @example '7bit'
   */

  this.encoding = "";
  /**
   * Mime type of the file
   * @example 'application/x-zip-compressed'
   */

  this.mimetype = "";
  /**
   * The folder to which the file has been saved
   * @example 'uploads/'
   */

  this.destination = "";
  /**
   * The name of the file within the destination
   * @example '0fcd5d6a843610d032574a37bcbd006f'
   */

  this.filename = "";
  /**
   * The full path to the uploaded file
   * @example 'uploads\\0fcd5d6a843610d032574a37bcbd006f'
   */

  this.path = "";
  /**
   * Size of the file in bytes
   * @example 258278
   */

  this.size = 0;
};

exports.MulterStructure = MulterStructure;