"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ResponseReply =
/*#__PURE__*/
function () {
  function ResponseReply() {
    _classCallCheck(this, ResponseReply);
  }

  _createClass(ResponseReply, null, [{
    key: "success",

    /**
     * 
     * @param {*} responseData 
     * @param {String} message 
     */
    value: function success() {
      var responseData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var message = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      return {
        success: true,
        resultSuccess: true,
        resultMessage: "" + message,
        resultData: responseData
      };
    }
    /**
     * 
     * @param {String} message 
     */

  }, {
    key: "error",
    value: function error() {
      var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
      return {
        success: false,
        resultSuccess: false,
        resultMessage: "" + message,
        resultData: {}
      };
    }
  }]);

  return ResponseReply;
}();

var _default = ResponseReply;
exports.default = _default;