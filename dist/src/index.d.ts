import { FilterQuery, UpdateQuery, FindAndModifyWriteOpResultObject, OptionalId, ClientSession, DeleteWriteOpResultObject, UpdateWriteOpResult, InsertOneWriteOpResult, UpdateOneOptions, UpdateManyOptions, MongoClient } from 'mongodb'
import { AxiosPromise, AxiosRequestConfig } from 'axios';
import fsExtra from 'fs-extra';
import FormDataInstance from 'form-data'
import shelljsInstance from 'shelljs'
import dayjsInstance from 'dayjs';
import axiosInstance from 'axios';
import CryptoJSInstance from 'crypto-js'

export const fs = fsExtra;
export const FormData = FormDataInstance;
export const shelljs = shelljsInstance;
export const dayjs = dayjsInstance;
export const axios = axiosInstance;
export const CryptoJS = CryptoJSInstance;

export const ENV: JSON;

export const ConnectMongo = {
    initial = async function (): void { },
    getDB = function (): InterfaceDBAction { }
};


export const AppApi = {
    initial: Function(),
    addPostMethod: async function (uri: string, processor: Processor, isRequiredToken: Boolean = false): void { },
    /**
    * @description returns { __buffer: BYTE ARRAY, __httpStatus: IF NOT SUCCESS}
    */
    addPostMethodBufferResponse: async function (uri: string, processor: Processor, isRequiredToken: Boolean = false): void { },
    addGetMethod: async function (uri: string, processor: Processor, isRequiredToken: Boolean = false): void { },
    /**
    * @description returns { __buffer: BYTE ARRAY, __httpStatus: IF NOT SUCCESS}
    */
    addGetMethodBufferResponse: async function (uri: string, processor: Processor, isRequiredToken: Boolean = false): void { },
    addGetMethodRawResponse: async function (uri: string, processor: Processor, isRequiredToken: Boolean = false): void { },
    addUploadMultipartPostMethod: async function (uri: string, processor: Processor, isRequiredToken: Boolean = false ,config:AppApiUploadOptionalConfig ): void { },
    addDownloadGetMethod: async function (uri: string, processor: Processor, isRequiredToken: Boolean = false): void { },
    startApp: function (appName?:String,version?:String,description?:String): void { }
};

declare namespace Network {
    function requestPost(url: String, params: JSON = {}, config: AxiosRequestConfig = {}): Promise<JSON>;

    function requestGet(urlWithParams: String, config: AxiosRequestConfig = {}): Promise<JSON>;

    function requestDownloadGet(urlWithParams: String, fileOutput: String, config: AxiosRequestConfig = {}): Promise<JSON>;

    function requestDownloadPost(url: String, fileOutput: String, params: JSON = {}, config: AxiosRequestConfig = {}): Promise<JSON>;
}


declare namespace ZipUtils {

    /**
    * @description ZipUtils.zip(`/xx/floder`,`/output/myzip.zip`)
    */
    function zip(sourceFloderPath: String, destinationWithFieName: String, options?: ZipOptions): Promise<String>;

    /**
    * @description ZipUtils.unzip("/tmp/myzip.zip",/tmp/output)
    */
    function unzip(zipFilePath: String, extractToPath: String, options?: ZipOptions): Promise<String>;
}


declare namespace GenUtils {
    function genUUIDV4(): String;
    function genCleanUUIDV4(): String;
}

declare namespace TokenUtils {

    function generateToken(payload: TokenInfo,yourSecretKey: String=""): String;
    function decodeToken(token: String,yourSecretKey: String=""): TokenInfo;

}

declare namespace FileUtils {
    /**
     * @description ใช้แกะหาไฟล์ที่ถูกอัปโหลดมา
     * 
     */
    function getUploadFile(sv: ServiceData): MulterStructure;
}



/***
 * 
 * 
 * External class
 */

 class AppApiUploadOptionalConfig {
    fileuploadKeyName:String
 }
 class TokenInfo {

    username: String
    /**
     * @type {'admin' | 'user'}
     */
    role: String
    create_time: Number
    data_access_expiration_time: Number
    expiresIn: Number
    data: JSON

}

class MulterStructure {

    /**
     * @description Field name specified in the form
     */
    fieldname: String;
    /**
     * @description Name of the file on the user's computer
     * example 'Desktop.zip'
     */
    originalname: String;
    /**
     * @description Encoding type of the file
     * example '7bit'
     */
    encoding: String;
    /**
     * @description Mime type of the file
     * example 'application/x-zip-compressed'
     */
    mimetype: String;
    /**
     * @description The folder to which the file has been saved
     * example 'uploads/'
     */
    destination: String;
    /**
     * @description The name of the file within the destination
     * example '0fcd5d6a843610d032574a37bcbd006f'
     */
    filename: String;
    /**
     * @description The full path to the uploaded file
     * example 'uploads\\0fcd5d6a843610d032574a37bcbd006f'
     */
    path: String;
    /**
     * @description Size of the file in bytes
     * example 258278
     */
    size: number;
}

class ZipOptions {
    password: String;
}

class ServiceData {
    body: JSON;
    headers: JSON;
    token: TokenInfo;
}
class Processor {
    process(sv: ServiceData): JSON { }
}

class InterfaceDBAction {

    /**
     * Select * From
     * @param {String} collectionName 
     * @param {*} queryCondition 
     * @param {Object} filter
     */
    async find(collectionName, queryCondition = {}, filter = {}): Promise<JSON[]>;
    /**
     * Select * From Where Only One Data
     * @param {String} collectionName 
     * @param {*} queryCondition 
     * @param {Object} filter
     */
    async findOne(collectionName, queryCondition = {}, filter = {}): JSON

    /**
   * Insert One Data
   * @param {String} collectionName 
   * @param {OptionalId<Object>} data 
   * @param {ClientSession} session
   */
    async insertOne(collectionName, data = {}, session = undefined): Promise<InsertOneWriteOpResult<any>>

    /**
     * Update Data
     * @param {String} collectionName 
     * @param {FilterQuery<Object>} queryCondition 
     * @param {*} updateValue 
     * @param {UpdateOneOptions} options
     */
    async updateOne(collectionName, queryCondition = {}, updateValue = {}, options?: UpdateOneOptions): Promise<UpdateWriteOpResult>;


    /**
     * Update Many Data
     * @param {String} collectionName 
     * @param {*} queryCondition 
     * @param {*} updateValue 
     * @param {UpdateManyOptions} options
     */
    async updateMany(collectionName, queryCondition = {}, updateValue = {}, options?: UpdateManyOptions): Promise<UpdateWriteOpResult>;

    /**
     * Delete From
     * @param {String} collectionName 
     * @param {*} queryCondition 
     * 
     */
    async deleteMany(collectionName, queryCondition): Promise<DeleteWriteOpResultObject>;


    /**
    * @description Find a document and update it in one atomic operation. Requires a write lock for the duration of the operation.
    * http://mongodb.github.io/node-mongodb-native/3.3/api/Collection.html#findOneAndUpdate
    * @param {String} collectionName 
    * @param {FilterQuery<Object>} queryCondition 
    * @param {UpdateQuery<Object>} updateValue 
    *  
    * @returns {FindAndModifyWriteOpResultObject<Object>}
    */
    async findOneAndUpdate(collectionName, queryCondition = {}, updateValue = {}): Promise<FindAndModifyWriteOpResultObject<JSON>>;



    /**
     * @description https://docs.mongodb.com/manual/reference/operator/aggregation-pipeline
     * @param {String} collectionName 
     * @param {Object[]} pipeline 
     */
    async aggregate(collectionName, pipeline = {}): Promise<Array>;


    /**
     * @returns {ClientSession}
     */
    async CreateSession(): ClientSession;

    /**
     * @returns {MongoClient}
     */
    async getConnectionMongoClient():MongoClient;


}