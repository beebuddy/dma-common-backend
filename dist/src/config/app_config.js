"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _system_config = _interopRequireDefault(require("./system_config"));

var _package = _interopRequireDefault(require("../../package.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var appConfig = {
  ENV: _system_config.default.PROPS,
  APP_START_PORT: _system_config.default.APP_START_PORT,
  appName: _package.default.name,
  version: _package.default.version,
  description: _package.default.description,
  config: {
    db_connection_pool_size: _system_config.default.DB_CONNECTION_POOL_SIZE || 1,
    DB_DISABLE_BASEFIELD: _system_config.default.DB_DISABLE_BASEFIELD == 'true' ? true : false,
    DB_URL: _system_config.default.DB_URL,
    DB_NAME: _system_config.default.DB_NAME,
    DB_PORT: _system_config.default.DB_PORT,
    DB_USERNAME: _system_config.default.DB_USERNAME,
    DB_PASSWORD: _system_config.default.DB_PASSWORD,
    TOKEN_SECRET_KEY: _system_config.default.TOKEN_SECRET_KEY
  },
  TokenExpiredTime: {
    value: 10,
    comment: "minute"
  },
  project_dir: __dirname
};
var _default = appConfig;
exports.default = _default;