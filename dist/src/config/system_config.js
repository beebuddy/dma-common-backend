"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _dotenv = _interopRequireDefault(require("dotenv"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** ------------ Import domain config ------------ */
_dotenv.default.config();
/** Start of Config */
// Mongo DB Config


var DB_URL = process.env.DB_URL;
var DB_NAME = process.env.DB_NAME;
var DB_PORT = process.env.DB_PORT;
var DB_USERNAME = process.env.DB_USERNAME;
var DB_PASSWORD = process.env.DB_PASSWORD; // Config System

var DB_CONNECTION_POOL_SIZE = process.env.DB_CONNECTION_POOL_SIZE;
var DB_DISABLE_BASEFIELD = process.env.DB_DISABLE_BASEFIELD;
var API_DISABLE_LOG_BODY = process.env.API_DISABLE_LOG_BODY || "N";
var API_DISABLE_LOG_HEADER = process.env.API_DISABLE_LOG_HEADER || "N";
var APP_START_PORT = process.env.APP_START_PORT;
var TOKEN_SECRET_KEY = process.env.TOKEN_SECRET_KEY || "";
var PREFIX = "APP_";
var PROPS = {};
Object.keys(process.env).forEach(function (key) {
  if (key.indexOf(PREFIX) == 0) {
    PROPS[key] = process.env[key];
  }
});
var systemConfig = {
  API_DISABLE_LOG_BODY: API_DISABLE_LOG_BODY,
  API_DISABLE_LOG_HEADER: API_DISABLE_LOG_HEADER,
  DB_URL: DB_URL,
  DB_NAME: DB_NAME,
  DB_PORT: DB_PORT,
  DB_USERNAME: DB_USERNAME,
  DB_PASSWORD: DB_PASSWORD,
  DB_CONNECTION_POOL_SIZE: DB_CONNECTION_POOL_SIZE,
  DB_DISABLE_BASEFIELD: DB_DISABLE_BASEFIELD,
  APP_START_PORT: APP_START_PORT,
  TOKEN_SECRET_KEY: TOKEN_SECRET_KEY,
  PROPS: PROPS
};
var _default = systemConfig;
exports.default = _default;