import jwt from 'jwt-simple'
import TokenUtils from '../TokenUtils/TokenUtils';
import { Request, Response } from 'express';

const HEADER_TOKEN_KEY = 'token'
class Middleware {

    /**
     * @description "Middleware สำหรับตรวจสอบ Token"
     * @returns {Function}
     */
    static VerifyToken(isRequiredToken) {

        if (isRequiredToken && isRequiredToken == true) {
            /**
             * 
             * @param {Request} req 
             * @param {Response} res 
             * @param {*} next 
             */
            const verifyTokenMiddleWare = (req, res, next) => {
                try {
                    let tokenInfo = TokenUtils.decodeToken(req.headers[HEADER_TOKEN_KEY])
                    req.$token = tokenInfo;
                    if (tokenInfo?.create_time && tokenInfo?.expiresIn) {
                        // check expires time
                        let currentTime = new Date().getTime();
                        let calculatedTime = (currentTime - (tokenInfo?.create_time + tokenInfo?.expiresIn));
                        if (calculatedTime > 0) {
                            res.status(401).send("Token expired");
                            return;
                        }

                        // renew token
                        let renewToken = TokenUtils.generateToken({
                            ...tokenInfo,
                            create_time: currentTime
                        });

                        res.setHeader(HEADER_TOKEN_KEY, renewToken);
                    }

                    next();
                } catch (error) {
                    res.status(401).send("Invalid token")
                }
            }

            return verifyTokenMiddleWare;
        } else {

            const blankMiddleWare = (req, res, next) => {
                next();
            }
            return blankMiddleWare;
        }


    }
}

export default Middleware