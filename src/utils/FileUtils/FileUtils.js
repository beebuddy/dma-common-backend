import fs from 'fs-extra';
import ServiceData from '../../common/ServiceData';
export default class FileUtils{

    /**
     * 
     * @param {ServiceData} sv
     * @returns {MulterStructure} 
     */
    static getUploadFile(sv){
        /**
         * @type {MulterStructure}
         */
        const uploadFile = sv.req?.file;
        return uploadFile;
    }
    
}