import jwt from 'jwt-simple'
import appConfig from '../../config/app_config';

const secretKey = appConfig.config.TOKEN_SECRET_KEY || "!qAzxcv,!@#x";

export default class TokenUtils{

    /**
     * @param {TokenInfo} payload 
     * @param {String} yourSecretKey
     */
    static generateToken(payload,yourSecretKey="") {
        return jwt.encode(payload, yourSecretKey|| secretKey);
    }

    /**
     * @param {String} token
     * @param {String} yourSecretKey
     * @returns {TokenInfo}
     */
    static decodeToken(token,yourSecretKey="") {
        return jwt.decode(token, yourSecretKey||secretKey);
    }

}


export class TokenInfo {
    constructor() {
        this.username = "";
        /**
         * @type {'admin' | 'user'}
         */
        this.role = "";
        this.create_time = 1578108112
        this.data_access_expiration_time = 1578108112
        this.expiresIn = 5888
        this.identityFacebook =""
        this.data = {}
    }
}
