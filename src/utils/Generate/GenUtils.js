
import uuidv4 from 'uuid/v4';
class GenUtils {
    /**
     * @description Ex. 7f1ab2d0-4575-4b01-92e1-486b977a0570
     * @returns {String}
     */
    static genUUIDV4() {
        return uuidv4();
    }

    /**
     * @description Ex. c49a9c513a29477abf0632e8b66385b4
     * @returns {String}
     */
    static genCleanUUIDV4() {
        /**
         * @type {String}
         */
        let uuid = uuidv4();
        uuid = uuid.replace(/-/g,"");
        return uuid;
    }

}

export default GenUtils;