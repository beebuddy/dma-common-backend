import os from "os";
import shelljs from "shelljs";
import fs from "fs-extra";
class ZipUtils {


    /**
     * @description ZipUtils.zip(`/xx/floder`,`/output/myzip.zip`)
     * @param {String} sourceFloderPath 
     * @param {String} destinationWithFieName 
     * @param {ZipUtils_ZipOptions} options
     * @returns {String}
     */
    static async zip(sourceFloderPath, destinationWithFieName, options) {

        checkOSSupport();

        let splitSourcePath = sourceFloderPath.split(`/`);
        if (splitSourcePath.length <= 1) {
            throw `You can't zip from root path`
        }

        let sourceDir = splitSourcePath[splitSourcePath.length - 1];

        let splitDestinationWithFieName = destinationWithFieName.split(`/`);
        if (splitDestinationWithFieName.length <= 0) {
            throw `Invalid destinationWithFieName`
        }

        let fileName = splitDestinationWithFieName[splitDestinationWithFieName.length - 1];
        let outputDir = destinationWithFieName.replace(fileName, '')

        // check has password
        if (options?.password) {
            return this._zipWithPassword(sourceFloderPath, outputDir, fileName, options?.password);
        } else {
            return this._zip(sourceFloderPath, sourceDir, outputDir, fileName);
        }


    }

    /**
     * @description ZipUtils.zip('/xx/floder',floder,'/output/','myzip.zip')
     * @param {String} sourcePath 
     * @param {String} fromFloderName
     * @param {String} destinationFolder 
     * @param {String} zipFileName 
     * @returns {String}
     */
    static async _zip(sourcePath, fromFloderName, destinationFolder, zipFileName) {
        let zipFilePath = destinationFolder + zipFileName;

        fs.ensureDirSync(destinationFolder);

        let exec_result = shelljs.exec(`cd ${sourcePath} && zip -r ${zipFilePath} .`);
        if (exec_result.stderr.length > 0) {
            throw exec_result.stderr;
        }

        return zipFilePath;
    }

    /**
     * 
     * @param {String} sourcePath 
     * @param {String} destinationFolder 
     * @param {String} zipFileName 
     * @param {String} password 
     * @returns {String}
     */
    static async _zipWithPassword(sourcePath, destinationFolder, zipFileName, password) {

        let zipFilePath = destinationFolder + zipFileName;

        fs.ensureDirSync(destinationFolder);

        let exec_result = shelljs.exec(`cd ${sourcePath} && zip --password ${password} -r ${zipFilePath} .`);
        if (exec_result.stderr.length > 0) {
            throw exec_result.stderr;
        }

        return zipFilePath;
    }

    /**
     * @description ZipUtils.unzip("/tmp/myzip.zip",/tmp/output)
     * @param {String} zipFilePath 
     * @param {String} extractToPath 
     * @param {ZipUtils_ZipOptions} options
     * @returns {String}
     */
    static async unzip(zipFilePath, extractToPath, options) {

        checkOSSupport();

        let exec_result = "";

        if (options?.password) {
            exec_result = shelljs.exec(`unzip -o -P ${options.password} ${zipFilePath} -d ${extractToPath}`);
        } else {
            exec_result = shelljs.exec(`unzip -o ${zipFilePath} -d ${extractToPath}`);
        }

        if (exec_result.stderr.length > 0) {
            throw exec_result.stderr;
        }

        return extractToPath;

    }

}

export default ZipUtils;

const checkOSSupport = () => {
    if(os.platform() === "win32") {
        throw "Unsupported module";
    }
}

class ZipUtils_ZipOptions {
    constructor() {
        this.password = "";
    }
}