import COLLECTION from "./collections";
import * as dbActions from "./db_config/action";

class _ConnectMongo {

  constructor(){
    
  }

  initial(){
    dbActions.openConnection();
  }
  getDB(){
    return dbActions;
  }

}
const ConnectMongo = new _ConnectMongo();
export default ConnectMongo;