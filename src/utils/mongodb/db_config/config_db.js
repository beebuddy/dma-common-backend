import appConfig from "../../../config/app_config";

// Config Database

const DB_USERNAME = encodeURIComponent(appConfig.config.DB_USERNAME);
const DB_PASSWORD = encodeURIComponent(appConfig.config.DB_PASSWORD);
const DB_NAME = appConfig.config.DB_NAME;
const DB_PORT = appConfig.config.DB_PORT;
const DB_URL = appConfig.config.DB_URL;

const DB_CONNECT_STR = `mongodb://${DB_USERNAME}:${DB_PASSWORD}@${DB_URL}:${DB_PORT}/${DB_NAME}`;

export default {
  DB_CONNECT_STR : DB_CONNECT_STR,
  DB_NAME : DB_NAME,
}