import systemConfig from "./system_config";
import packageJson from "../../package.json";

const appConfig = {
  ENV: systemConfig.PROPS,
  APP_START_PORT: systemConfig.APP_START_PORT,


  appName: packageJson.name,
  version: packageJson.version,
  description: packageJson.description,
  config: {
    db_connection_pool_size: systemConfig.DB_CONNECTION_POOL_SIZE || 1,
    DB_DISABLE_BASEFIELD : systemConfig.DB_DISABLE_BASEFIELD == 'true' ? true : false,
    DB_URL: systemConfig.DB_URL,
    DB_NAME : systemConfig.DB_NAME,
    DB_PORT : systemConfig.DB_PORT,
    DB_USERNAME : systemConfig.DB_USERNAME,
    DB_PASSWORD : systemConfig.DB_PASSWORD,
    TOKEN_SECRET_KEY : systemConfig.TOKEN_SECRET_KEY
  },
  TokenExpiredTime: {
    value: 10,
    comment: "minute"
  },
  project_dir: __dirname
}

export default appConfig;

