/** ------------ Import domain config ------------ */
import dotenv from 'dotenv'

dotenv.config();
/** Start of Config */


// Mongo DB Config
const DB_URL = process.env.DB_URL;
const DB_NAME = process.env.DB_NAME;
const DB_PORT = process.env.DB_PORT;
const DB_USERNAME = process.env.DB_USERNAME;
const DB_PASSWORD = process.env.DB_PASSWORD; // Config System
const DB_CONNECTION_POOL_SIZE = process.env.DB_CONNECTION_POOL_SIZE;
const DB_DISABLE_BASEFIELD = process.env.DB_DISABLE_BASEFIELD;

const API_DISABLE_LOG_BODY = process.env.API_DISABLE_LOG_BODY || "N"
const API_DISABLE_LOG_HEADER = process.env.API_DISABLE_LOG_HEADER || "N"


const APP_START_PORT = process.env.APP_START_PORT;
const TOKEN_SECRET_KEY = process.env.TOKEN_SECRET_KEY || "";

const PREFIX = `APP_`
const PROPS = {
  
}

Object.keys( process.env ).forEach(key => {
    if( key.indexOf(PREFIX) == 0 ){
      PROPS[key] = process.env[key];
    }
});

const systemConfig = {
  API_DISABLE_LOG_BODY,
  API_DISABLE_LOG_HEADER,
  DB_URL,
  DB_NAME,
  DB_PORT,
  DB_USERNAME,
  DB_PASSWORD,
  DB_CONNECTION_POOL_SIZE,
  DB_DISABLE_BASEFIELD,
  APP_START_PORT,
  TOKEN_SECRET_KEY,
  PROPS
};

export default systemConfig;
