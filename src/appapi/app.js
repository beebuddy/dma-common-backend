import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import logger from "../utils/logger/logger";
import appConfig from "../config/app_config";
import uuidv4 from 'uuid/v4';
import responseTime from 'response-time';
import multer from "multer";


import ServiceData from '../common/ServiceData'
import ResponseReply from '../common/ResponseReply'
import Middleware from "../utils/Middleware/Middleware";
import systemConfig from "../config/system_config";

const app = express();

const TAG = "[Application] ";


export function initial() {

  app.use(cors());
  app.use(bodyParser.json({
    limit: '50mb'
  }));
  app.use((req, res, next) => {
    // req.headers.__reqId = "[" + uuidv4() + "] ";
    // req.specialType = "request";
    req.startTime = new Date().getTime(); // stamp time start inservice
    logger.info(`${TAG}Call "${req.originalUrl}"`);
    if (systemConfig.API_DISABLE_LOG_HEADER?.toLocaleUpperCase() != "Y") {
      logger.info(`${TAG}:: Request Header :: ${JSON.stringify(req.headers)}`);
    }
    if (systemConfig.API_DISABLE_LOG_BODY?.toLocaleUpperCase() != "Y") {
      logger.info(`${TAG}:: Request Data :: ${JSON.stringify(req.body)}`);
    }
    next();
  });

  app.use(responseTime((req, res, time) => {
    const totalTimeInService = time;

    if (res.statusCode == 200) {
      logger.success(TAG, `RESPONSE STATUS CODE : ${res.statusCode} , MESSAGE : Ok`)
    } else {
      logger.error(TAG, `RESPONSE STATUS CODE : ${res.statusCode} , MESSAGE : ${res.statusMessage}`)
    }

    if (totalTimeInService > 100) {
      logger.warning(TAG + "Total time in service [over 100ms] : ", Math.round(totalTimeInService), " ms")
    } else {
      logger.info(TAG + "Total time in service : ", Math.round(totalTimeInService), " ms")
    }
  }));
  /**
   * Show App Detail
   */
  app.all("/app/version", (req, res) => {
    res.send({
      appName: appConfig.appName,
      description: appConfig.description,
      version: appConfig.version
    })
  });
}


export function addPostMethod(uri, processor, isRequiredToken) {

  app.post(uri, Middleware.VerifyToken(isRequiredToken), async (req, res) => {
    try {
      const sv = new ServiceData();
      sv.setData(req, res);
      const createResult = await processor.process(sv);
      res.send(ResponseReply.success(createResult));
    } catch (error) {
      logger.error(error + '')
      res.send(ResponseReply.error(error));
    }
  });
}

export function addPostMethodBufferResponse(uri, processor, isRequiredToken) {

  app.post(uri, Middleware.VerifyToken(isRequiredToken), async (req, res) => {
    try {
      const sv = new ServiceData();
      sv.setData(req, res);
      const createResult = await processor.process(sv);
      if(createResult?.__httpStatus) {
        res.status(createResult?.__httpStatus).send();
      } else {
        res.end(createResult?.__buffer);
      }
    } catch (error) {
      logger.error(error + '')
      res.status(404).send(error)
    }
  });
}

export function addGetMethod(uri, processor, isRequiredToken) {
  app.get(uri, Middleware.VerifyToken(isRequiredToken), async (req, res) => {
    try {
      const sv = new ServiceData();
      sv.setData(req, res);
      sv.body = { ...req.query }
      const createResult = await processor.process(sv);
      res.send(ResponseReply.success(createResult));
    } catch (error) {
      logger.error(error + '')
      res.send(ResponseReply.error(error));
    }
  });
}

export function addGetMethodBufferResponse(uri, processor, isRequiredToken) {
  app.get(uri, Middleware.VerifyToken(isRequiredToken), async (req, res) => {
    try {
      const sv = new ServiceData();
      sv.setData(req, res);
      sv.body = { ...req.query }
      const createResult = await processor.process(sv);
      if(createResult?.__httpStatus) {
        res.status(createResult?.__httpStatus).send();
      } else {
        res.end(createResult?.__buffer);
      }
    } catch (error) {
      logger.error(error + '')
      res.status(404).send(error)
    }
  });
}

export function addGetMethodRawResponse(uri, processor, isRequiredToken) {
  app.get(uri, Middleware.VerifyToken(isRequiredToken), async (req, res) => {
    try {
      const sv = new ServiceData();
      sv.setData(req, res);
      sv.body = { ...req.query }
      const createResult = await processor.process(sv);
      res.send(createResult);
    } catch (error) {
      logger.error(error + '')
      res.send(ResponseReply.error(error));
    }
  });
}

export function addDownloadGetMethod(uri, processor, isRequiredToken) {
  app.get(uri, Middleware.VerifyToken(isRequiredToken), async (req, res) => {
    try {
      const sv = new ServiceData();
      sv.setData(req, res);
      sv.body = { ...req.query }
      const createResult = await processor.process(sv);
      res.download(createResult?.__downloadPath, (error) => {
        if(error){
          logger.error(`Error uri:${uri} =`+error);
          res.send(ResponseReply.error(error));
        }
      });
    } catch (error) {
      logger.error(error + '')
      res.send(ResponseReply.error(error));
    }
  });
}

const upload = multer({ dest: 'uploads/' })
export function addUploadMultipartPostMethod(uri, processor, isRequiredToken, appApiUploadOptionalConfig) {
  /**
   * @type {AppApiUploadOptionalConfig}
   */
  let optionsConfig = new AppApiUploadOptionalConfig();
  optionsConfig.fileuploadKeyName = appApiUploadOptionalConfig?.fileuploadKeyName || 'data';


  app.post(uri, Middleware.VerifyToken(isRequiredToken), upload.single(optionsConfig?.fileuploadKeyName), async (req, res) => {
    try {
      const sv = new ServiceData();
      sv.setData(req, res);
      const createResult = await processor.process(sv);
      res.send(ResponseReply.success(createResult));
    } catch (error) {
      logger.error(error + '')
      res.send(ResponseReply.error(error));
    }
  });
}

export function startApp(appName, version, description) {
  appConfig.appName = appName || appConfig.appName;
  appConfig.version = version || appConfig.version;
  appConfig.description = description || appConfig.description;
  const server = app.listen(appConfig.APP_START_PORT, () => {
    logger.info(`${TAG}Starting ${appConfig.appName} (version ${appConfig.version}) , Listening on port ${appConfig.APP_START_PORT} !`)
  });

  // Set timeout for server
  server.setTimeout(1000 * 60 * 30);

}

class AppApiUploadOptionalConfig {
  constructor() {
    this.fileuploadKeyName = ""
  }

}