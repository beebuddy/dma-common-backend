import dotenv from 'dotenv'
import ConnectMongoInstance from './utils/mongodb/connect_db';
import NetworkInstance from './utils/network/Network'
import ZipUtilsInstance from './utils/ZipUtils/ZipUtils'
import GenUtilsInstance from './utils/Generate/GenUtils'
import { initial, addPostMethod, addPostMethodBufferResponse, addGetMethodBufferResponse, addGetMethod,addGetMethodRawResponse, startApp ,addUploadMultipartPostMethod,addDownloadGetMethod } from './appapi/app'
import appConfig from './config/app_config';
import TokenUtilsInstance from './utils/TokenUtils/TokenUtils';
import FileUtilsInstance  from './utils/FileUtils/FileUtils';
import fsExtra from 'fs-extra';
import FormDataInstance from 'form-data';
import shelljsInstance from 'shelljs'
import dayjsInstance from 'dayjs';
import axiosInstance from 'axios';
import CryptoJSInstance from 'crypto-js'

dotenv.config();
export const fs = fsExtra;
export const FormData = FormDataInstance;
export const shelljs = shelljsInstance;
export const ENV = appConfig.ENV;
export const dayjs = dayjsInstance;
export const axios = axiosInstance;
export const CryptoJS = CryptoJSInstance;


export const Network = NetworkInstance
export const ZipUtils = ZipUtilsInstance
export const GenUtils = GenUtilsInstance
export const TokenUtils = TokenUtilsInstance
export const FileUtils = FileUtilsInstance;

export const ConnectMongo = ConnectMongoInstance;


export const AppApi = {
    initial, 
    addPostMethod, 
    addPostMethodBufferResponse,
    addGetMethod,
    addGetMethodBufferResponse,
    addGetMethodRawResponse,
    addUploadMultipartPostMethod,
    startApp,
    addDownloadGetMethod
};