class ResponseReply {

    /**
     * 
     * @param {*} responseData 
     * @param {String} message 
     */
    static success(responseData = {},message = ""){
      return {
        success: true,
        resultSuccess: true,
        resultMessage: "" + message,
        resultData: responseData
      };
    }
  
    /**
     * 
     * @param {String} message 
     */
    static error(message = ""){
      return {
        success: false,
        resultSuccess: false,
        resultMessage: "" + message,
        resultData: {}
      };
    }
  
  }
  
  export default ResponseReply;
