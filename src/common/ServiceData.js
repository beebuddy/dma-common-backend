export default class ServiceData {

    constructor() {}


    /**
     * 
     * @param {*} req 
     * @param {*} res 
     */
    setData(req, res) {
        this.body = req.body;
        this.headers = req.headers;
        this.req = req;
        this.res = res;
        this.token = req.$token;
    }

}


export class MulterStructure{
    constructor(){
        /**
         * Field name specified in the form
         * @example 'template'
         */
        this.fieldname = "";
        /**
         * Name of the file on the user's computer
         * @example 'Desktop.zip'
         */
        this.originalname = "";
        /**
         * Encoding type of the file
         * @example '7bit'
         */
        this.encoding = "";
        /**
         * Mime type of the file
         * @example 'application/x-zip-compressed'
         */
        this.mimetype = "";
        /**
         * The folder to which the file has been saved
         * @example 'uploads/'
         */
        this.destination = "";
        /**
         * The name of the file within the destination
         * @example '0fcd5d6a843610d032574a37bcbd006f'
         */
        this.filename = "";
        /**
         * The full path to the uploaded file
         * @example 'uploads\\0fcd5d6a843610d032574a37bcbd006f'
         */
        this.path = "";
        /**
         * Size of the file in bytes
         * @example 258278
         */
        this.size = 0;
    }
}
