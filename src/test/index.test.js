import "@babel/polyfill";
import dayjs from "dayjs";
import GenUtils from "../utils/Generate/GenUtils";
import { FindOneAndUpdateOption, CommonOptions, MongoClient, FilterQuery, UpdateQuery, FindAndModifyWriteOpResultObject, OptionalId, ClientSession, DeleteWriteOpResultObject, UpdateWriteOpResult, UpdateOneOptions, CollectionInsertOneOptions } from 'mongodb';
import { AppApi } from "..";
import TestAppApi from "./TestAppApi/TestAppApi";


(async () => {
  await TestAppApi();
})();