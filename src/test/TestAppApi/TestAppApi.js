import { AppApi, ENV, TokenUtils } from "../..";

export default async function TestAppApi() {

    console.log("ENV = ",ENV)
    // let token = TokenUtils.generateToken({
    //     username : "ter"
    // })
    // console.log("token = ",token)

    //console.log("decode =",TokenUtils.decodeToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InRlciJ9.oIc1zOgBaxJguDgX3FPlvs7Y_aaBQhLQvmWwRZGbtNA"))

    // intitial app middleware
    AppApi.initial();
    AppApi.addGetMethod(`/test1`, { process: (sv) => { console.log("It' working",sv.body); } },true);
    AppApi.addGetMethodRawResponse(`/fake`, { process: (sv) => { console.log("It' working",sv.body); return { "raw" : { x : "1",body : sv.body} } } });
    AppApi.addDownloadGetMethod(`/test2`, { process: (sv) => { console.log("It' working",sv.body); return {__downloadPath : "/Users/tanawat/Downloads/IMG_8611.PNG"} } });
    AppApi.startApp("test", "xxx");

}
